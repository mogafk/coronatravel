$(document).ready(function() {

    //mobile services
    if (!window.matchMedia("(min-width: 768px)").matches) {
        var descriptions = [],
            e_mDescription = $(".service_description-mobile p");
        $(".service_description").each(function(index, elem){
            console.log(elem.innerHTML)
            descriptions.push(elem.innerHTML);
        });


        $(".service img").on("click", function(){
            $(".service img").removeClass("service_image-active");
            $(this).addClass("service_image-active");
            e_mDescription.text(descriptions[$(".service img").index(this)]);
        });

        //preset
        $(".service img").eq(0).addClass("service_image-active");
        e_mDescription.text(descriptions[0]);
        //end preset
    }
    //end mobile services


    //popover bootstrap for founders init
    $(".author_image_wrap").popover({
        container: ".slide3",
        animation:true,
        content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis consequatur culpa in inventore ipsam ipsum laudantium libero minima, nostrum odio odit omnis perspiciatis placeat quibusdam tempora ullam, vel velit.",
        placement: "top"
    });
    //end popovers


    $('#fullpage').fullpage({
        css3: true,
        scrollingSpeed: 700,
        autoScrolling: true,
        scrollBar: false,
        easing: 'easeInOutCubic',
        easingcss3: 'ease',
        loopBottom: false,
        loopTop: false,
        loopHorizontal: true,
        continuousVertical: false,
        scrollOverflow: false,
        touchSensitivity: 15,
        normalScrollElementTouchThreshold: 5,
        keyboardScrolling: true,
        animateAnchor: true,
        recordHistory: true,
        verticalCentered: false,
        fixedElements: '.header',
        sectionSelector: '.section',
        slideSelector: '.slide',

        //events
        onLeave: function(index, nextIndex, direction){
            if(nextIndex == 2){
                if (window.matchMedia("(min-width: 768px)").matches) { //least
                    $(".service").addClass("hide");
                } else {//less
                    $("#header").addClass("hide");
                }
            }
            if(nextIndex == 1){
                if(!window.matchMedia("(min-width: 768px)").matches){
                    $("#header").removeClass("hide");
                }
            }
            if(nextIndex == 8){
                $("#header").addClass('animated fadeOut');
            }
        },
        afterLoad: function(anchorLink, index){
            if(index == 2){
                if (window.matchMedia("(min-width: 768px)").matches) {//least
                    $(".service").removeClass("hide");
                    $('.service').addClass('animated bounceInLeft');
                }
            }
            if(index == 3){//popovers founders
                if (window.matchMedia("(min-width: 768px)").matches) {//least
                    $(".author_image_wrap").popover("show");
                } else {//less
                    $(".author_image_wrap").eq(0).popover("show");
                }
            }
            if(index == 7){
                $("#header").removeClass("fadeOut").addClass("fadeIn");
            }
            if(index == 8){
            }
        }

    });
    $.fn.fullpage.reBuild();
});